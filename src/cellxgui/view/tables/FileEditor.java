/*
 * FileEditor.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 */
package cellxgui.view.tables;

import cellxgui.controller.CellXParameter;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import javax.swing.DefaultCellEditor;
import javax.swing.JFileChooser;
import javax.swing.JTable;
import javax.swing.JTextField;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class FileEditor extends DefaultCellEditor implements ActionListener {

    private final JTextField textField;
    private final JFileChooser fileChooser;
    protected static final String CHOOSE = "choose";
    protected static final String EDIT = "edit";
    private File newValue, oldValue;
    private CellXParameter p;

    public FileEditor() {
        super(new JTextField());
        textField = (JTextField) getComponent();
        fileChooser = new JFileChooser();
        fileChooser.setApproveButtonText("Set");
        fileChooser.setMultiSelectionEnabled(false);
        textField.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() > 1) {
                    actionPerformed(new ActionEvent(textField, 0, CHOOSE));
                }
            }
        });

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals(CHOOSE)) {
            if (oldValue != null && oldValue instanceof File) {
                fileChooser.setSelectedFile((File) oldValue);
            }
            final int r = fileChooser.showOpenDialog(textField);
            if (r == JFileChooser.APPROVE_OPTION) {
                newValue = fileChooser.getSelectedFile();
            } else {
                newValue = oldValue;
            }
            fireEditingStopped(); //Make the renderer reappear.
        }
    }

    @Override
    public Object getCellEditorValue() {
        if (newValue == null) {
            newValue = new File(textField.getText());
        }
        return newValue;

    }

    //Implement the one method defined by TableCellEditor.
    @Override
    public Component getTableCellEditorComponent(JTable table,
            Object value,
            boolean isSelected,
            int row,
            int column) {
        if (value != null && value instanceof File) {
            oldValue = (File) value;
            textField.setText(oldValue.toString());
        } else {
            oldValue = null;
        }
        newValue = null;

        return textField;
    }

    public void setParam(CellXParameter p) {
        this.p = p;
    }
}
