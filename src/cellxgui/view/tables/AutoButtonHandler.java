/*
 * AutoButtonHandler.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.view.tables;

import cellxgui.controller.CellXController;
import cellxgui.controller.CellXGuiConfiguration;
import cellxgui.controller.CellXParameter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class AutoButtonHandler implements ActionListener {

    private final CellXController control;
    private CellXTableView tableView;

    public AutoButtonHandler(CellXController control, CellXTableView view) {
        this.control = control;
        this.tableView = view;
    }

    public void setTableView(CellXTableView tableView) {
        this.tableView = tableView;
    }

    boolean hasAutoMethod(CellXParameter p) {
        if (p.equals(CellXGuiConfiguration.getInstance().cropRegionBoundaryParameter)
                || p.equals(CellXGuiConfiguration.getInstance().maximumCellLengthParameter)
                || p.equals(CellXGuiConfiguration.getInstance().membraneIntensityProfileParameter)
                || p.equals(CellXGuiConfiguration.getInstance().membraneLocationParameter)
                || p.equals(CellXGuiConfiguration.getInstance().membraneWidthParameter)
                || p.equals(CellXGuiConfiguration.getInstance().seedRadiusLimitParameter)) {
            return true;
        }
        return false;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        final int selectedRow = tableView.getSelectedRow();
        final CellXParameter p = CellXGuiConfiguration.getParameters().get(selectedRow);
        if (p.equals(CellXGuiConfiguration.getInstance().cropRegionBoundaryParameter)) {
            control.autoCropRegion();
        } else if (p.equals(CellXGuiConfiguration.getInstance().maximumCellLengthParameter)) {
            control.autoMaximumCellLength();
        } else if (p.equals(CellXGuiConfiguration.getInstance().membraneIntensityProfileParameter)) {
            control.autoMembraneIntensityProfile();
        } else if (p.equals(CellXGuiConfiguration.getInstance().membraneLocationParameter)) {
            control.autoMembraneLocation();
        } else if (p.equals(CellXGuiConfiguration.getInstance().membraneWidthParameter)) {
            control.autoMembraneWidth();
        } else if (p.equals(CellXGuiConfiguration.getInstance().seedRadiusLimitParameter)) {
            control.autoSeedRadiusLimit();
        }
    }
}
