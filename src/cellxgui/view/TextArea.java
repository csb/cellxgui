/*
 * TextArea.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.view;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JTextArea;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class TextArea extends JTextArea {

    private Color background;

    public TextArea() {
        setEditable(false);
        setWrapStyleWord(true);
        background = getBackground();
        super.setBackground(new Color(0, 0, 0, 0));
    }

    @Override
    protected void paintComponent(Graphics g) {
        g.setColor(background);
        g.fillRect(0, 0, this.getWidth(), this.getHeight());
        super.paintComponent(g);
    }

    @Override
    public void setBackground(Color bg) {
        this.background = bg;
    }
}
