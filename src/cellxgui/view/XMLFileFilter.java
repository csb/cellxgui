/*
 * XMLFileFilter.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.view;

import java.io.File;
import javax.swing.filechooser.FileFilter;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class XMLFileFilter extends FileFilter {

    public static final String FILE_EXTENSION = String.valueOf(".xml");

    @Override
    public boolean accept(File f) {
        return f.isDirectory() || f.toString().endsWith(FILE_EXTENSION);
    }

    @Override
    public String getDescription() {
        return String.valueOf("XML file (" + FILE_EXTENSION + ")");
    }

    public FileFilter getFileFilter() {
        return this;
    }
}
