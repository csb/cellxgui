/*
 * FlatFieldRegexpWizard.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.view.dialogs.wizard;

import cellxgui.view.CellXView;
import java.io.File;
import java.util.regex.Pattern;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class FlatFieldWizard extends Wizard implements WizardWithFilename {

    private final FlatFieldModel model;

    public FlatFieldWizard(CellXView view, String fluoGroup, File imageDir) {
        super(view);
        this.model = new FlatFieldModel(fluoGroup);
        initWizardPanels(imageDir);
    }

    @Override
    protected void registerWizardPanel(WizardPanel wp) {
        super.registerWizardPanel(wp);
        model.addPropertyChangeListener(wp);
    }

    public static void main(String args[]) {
        /*
         * Set the Nimbus look and feel
         */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the
         * default look and feel. For details see
         * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
        } catch (InstantiationException ex) {
        } catch (IllegalAccessException ex) {
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
        }
        //</editor-fold>

        /*
         * Create and display the dialog
         */
        java.awt.EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {

                final FlatFieldWizard dialog = new FlatFieldWizard(null, "(gfp|yfp)", null);
                dialog.setLocationRelativeTo(null);

                dialog.addWindowListener(new java.awt.event.WindowAdapter() {

                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.showWizard();
            }
        });
    }

    private void initWizardPanels(File imageDir) {
        final SelectFileWP p1 = new SelectFileWP(this);
        p1.setText("Please add all regular expressions for fluorescence types before continuing.\n\n"
                + "Enter a flat field image filename:");
        p1.setFileDialogDirectory(imageDir);

        final FlatFieldExtractFluoWP p2 = new FlatFieldExtractFluoWP(this);

        final FlatFieldIdentifyWP p3 = new FlatFieldIdentifyWP(this);

        p1.setNextWizardPanel(p2);
        p2.setNextWizardPanel(p3);

        p2.setPeviousWizardPanel(p1);
        p3.setPeviousWizardPanel(p2);

        registerWizardPanel(p2);
        registerWizardPanel(p3);
        setInitialPanel(p1);
    }

    @Override
    public void setFileName(String name) {
        model.setFileName(name);
    }

    public void setFlatFieldIdentifyRegion(int selectionStart, int selectionEnd) {
        model.setFlatFieldIdentifyReagion(selectionStart, selectionEnd);
    }

    public void setFluorescenceTypeRegion(int selectionStart, int selectionEnd) {
        model.setFluorescenceTypeRegion(selectionStart, selectionEnd);
    }

    public FlatFieldModel getModel() {
        return model;
    }

    public Pattern getRegularExpression() {
        return model.generatePattern();
    }
}
