package cellxgui.view.dialogs.wizard;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public interface WizardWithFilename {

    public void setFileName(String name);
}
