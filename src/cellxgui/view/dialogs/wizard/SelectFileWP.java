/*
 * SelectFileWizardPanel.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.view.dialogs.wizard;

import java.io.File;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class SelectFileWP extends AbstractFileSelectorWizardPanel {

    private final WizardWithFilename wizard;

    public SelectFileWP(WizardWithFilename wizard) {
        this.wizard = wizard;
    }

    @Override
    public boolean validData() {
        if (textField.getText().trim().isEmpty()) {
            showErrorMessage("Filename missing");
            return false;
        }
        return true;
    }

    @Override
    public void commitData() {
        wizard.setFileName(new File(textField.getText()).getName());
    }

    @Override
    public void beforeShowing() {
        hideErrorMessage();
    }

    @Override
    public void afterShowing() {
        textField.requestFocus();
    }
}
