/*
 * FlatFieldIdentifyWizardPanel.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.view.dialogs.wizard;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class FlatFieldIdentifyWP extends AbstractSelectionWizardPanel {

    private final FlatFieldWizard wizard;

    public FlatFieldIdentifyWP(FlatFieldWizard wizard) {
        this.wizard = wizard;
        setText("\n\nSelect part of the filename that identifies this file as a flat field image:");
        validate();
    }

    @Override
    public boolean validData() {
        if (textField.getSelectedText() == null) {
            showErrorMessage("Selection required");
            return false;
        }

        if (wizard.getModel().isRegionOverlap(textField.getSelectionStart(), textField.getSelectionEnd())) {
            showErrorMessage("Region must not overlap with previous region");
            return false;
        }


        return true;
    }

    @Override
    public void commitData() {
        wizard.setFlatFieldIdentifyRegion(textField.getSelectionStart(), textField.getSelectionEnd());
    }

    @Override
    public void beforeShowing() {
        hideErrorMessage();
    }

    @Override
    public void afterShowing() {
        textField.requestFocus();
    }
}
