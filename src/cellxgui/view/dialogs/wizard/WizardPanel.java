/*
 * WizardPanel.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.view.dialogs.wizard;

import java.beans.PropertyChangeListener;
import javax.swing.JPanel;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
abstract public class WizardPanel extends JPanel implements PropertyChangeListener {

    private WizardPanel nextPanel;
    private WizardPanel prevPanel;

    public WizardPanel getNextWizardPanel() {
        return nextPanel;
    }

    public void setNextWizardPanel(WizardPanel wp) {
        nextPanel = wp;
    }

    public WizardPanel getPreviousWizardPanel() {
        return prevPanel;
    }

    public void setPeviousWizardPanel(WizardPanel wp) {
        prevPanel = wp;
    }

    public boolean hasPreviousPanel() {
        return prevPanel != null;
    }

    public boolean isFinalPanel() {
        return nextPanel == null;
    }

    abstract public void beforeShowing();

    abstract public void afterShowing();

    abstract public boolean validData();

    abstract public void commitData();
}
