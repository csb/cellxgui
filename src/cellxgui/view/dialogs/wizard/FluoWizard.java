package cellxgui.view.dialogs.wizard;

import cellxgui.view.CellXView;
import java.io.File;
import java.util.regex.Pattern;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class FluoWizard extends Wizard implements WizardWithFilename {

    private String filename;
    private int selectionStart, selectionEnd;

    public FluoWizard(CellXView view, File imageDir) {
        super(view);
        initWizardPanels(imageDir);
    }

    @Override
    public void setFileName(String name) {
        firePropertyChange(FlatFieldModel.FILENAME_PROPERTY, filename, name);
        this.filename = name;
    }

    @Override
    protected void registerWizardPanel(WizardPanel wp) {
        super.registerWizardPanel(wp);
        addPropertyChangeListener(wp);
    }

    public void setSelectedRegion(int start, int end) {
        this.selectionStart = start;
        this.selectionEnd = end;
    }

    private void initWizardPanels(File imageDir) {
        final SelectFileWP p1 = new SelectFileWP(this){
            @Override
            public boolean validData() {
                if (!super.validData()) {
                    return false;
                }
                if (!textField.getText().matches(".*?\\d+.*")) {
                    showErrorMessage("Filename must contain the frame number");
                    return false;
                }
                return true;
            }
        };
        p1.setFileDialogDirectory(imageDir);
        p1.setText("Enter fluorescence image filename:");
        
        
        final AbstractSelectionWizardPanel p2 = new AbstractSelectionWizardPanel() {

            @Override
            public void beforeShowing() {
                hideErrorMessage();
            }

            @Override
            public void afterShowing() {
                textField.requestFocus();
            }

            @Override
            public boolean validData() {
                if (textField.getSelectedText() == null) {
                    showErrorMessage("Selection required");
                    return false;
                }
                return true;
            }

            @Override
            public void commitData() {
                setSelectedRegion(textField.getSelectionStart(), textField.getSelectionEnd());
            }
        };
        p2.setText("Select the part of the filename that encodes the fluorescence type:");

        p1.setNextWizardPanel(p2);
        p2.setPeviousWizardPanel(p1);

        registerWizardPanel(p2);
        setInitialPanel(p1);
    }

    public Pattern getPattern() {
        return Pattern.compile(generateFluoPattern(filename, selectionStart, selectionEnd));
    }

    public String generateFluoPattern(String filename, int selectionStart, int selectionEnd) {
        String selected = filename.substring(selectionStart, selectionEnd);
        selected = selected.replaceAll("\\.", "\\\\.");
        final StringBuilder sb = new StringBuilder();       
        if(selectionStart==0){
            sb.append("^");
        }else{
            sb.append(".*?");       
        }
        final int dotIdx = filename.lastIndexOf('.');
        if (selectionEnd > dotIdx) {
            sb.append("(");
            sb.append(selected);
            sb.append(")");     
            if( selectionEnd!=filename.length() ){
                sb.append(".*");
            }           
        } else {            
            sb.append("(");
            sb.append(selected);
            sb.append(").*?");         
            sb.append(OofWizard.getExtensionPattern(filename));
        }
        return sb.toString();
    }
}
