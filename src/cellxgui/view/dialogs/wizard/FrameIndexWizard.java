package cellxgui.view.dialogs.wizard;

import cellxgui.view.CellXView;
import java.io.File;
import java.util.regex.Pattern;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class FrameIndexWizard extends Wizard implements WizardWithFilename {

    private String filename;
    private int selectionStart, selectionEnd;

    public FrameIndexWizard(CellXView view, File imageDir) {
        super(view);
        initWizardPanels(imageDir);
    }

    @Override
    public void setFileName(String name) {
        firePropertyChange(FlatFieldModel.FILENAME_PROPERTY, filename, name);
        this.filename = name;
    }

    @Override
    protected void registerWizardPanel(WizardPanel wp) {
        super.registerWizardPanel(wp);
        addPropertyChangeListener(wp);
    }

    public void setSelectedRegion(int start, int end) {
        this.selectionStart = start;
        this.selectionEnd = end;
    }

    private void initWizardPanels(File imageDir) {


        final SelectFileWP p1 = new SelectFileWP(this) {

            @Override
            public boolean validData() {
                if (!super.validData()) {
                    return false;
                }
                if (!textField.getText().matches(".*?\\d+.*")) {
                    showErrorMessage("Filename must contain the frame number");
                    return false;
                }
                return true;
            }
        };
        p1.setFileDialogDirectory(imageDir);
        p1.setText("Enter out of focus image filename:");

        final AbstractSelectionWizardPanel p2 = new AbstractSelectionWizardPanel() {

            @Override
            public void beforeShowing() {
                hideErrorMessage();
            }

            @Override
            public void afterShowing() {
                textField.requestFocus();
            }

            @Override
            public boolean validData() {
                if (textField.getSelectedText() == null) {
                    showErrorMessage("Selection required");
                    return false;
                }
                if (!textField.getSelectedText().matches("\\d+")) {
                    showErrorMessage("Selection must contain digits only");
                    return false;
                }
                final String charBefore = filename.substring(textField.getSelectionStart() - 1, textField.getSelectionStart());
                if (charBefore.matches("\\d")) {
                    showErrorMessage("No digit allowed immediately before selection");
                    return false;
                }
                return true;
            }

            @Override
            public void commitData() {
                setSelectedRegion(textField.getSelectionStart(), textField.getSelectionEnd());
            }
        };
        p2.setText("Select the digits in the filename that represent the frame number:");

        p1.setNextWizardPanel(p2);
        p2.setPeviousWizardPanel(p1);

        registerWizardPanel(p2);
        setInitialPanel(p1);
    }

    public Pattern getPattern() {
        return Pattern.compile(generateFramePattern(filename, selectionStart, selectionEnd));
    }

    public static String generateFramePattern(String filename, int start, int end) {
        final int dGroupCount = countDigitGroupsBefore(filename, start);
        final StringBuilder sb = new StringBuilder();

        if (dGroupCount == 0) {
            sb.append(".*?(\\d+).*?");
            sb.append(OofWizard.getExtensionPattern(filename));
            return sb.toString();
        }

        sb.append("\\S*\\d*");
        for (int i = 1; i < dGroupCount; ++i) {
            sb.append("\\D*\\d*");
        }
        sb.append("\\D+");
        sb.append("(\\d+).*?");
        sb.append(OofWizard.getExtensionPattern(filename));

        return sb.toString();
    }

    public static int countDigitGroupsBefore(String fname, int start) {
        boolean inside = false;
        int count = 0;
        for (int i = 0; i < start; ++i) {
            final String c = fname.substring(i, i + 1);
            if (c.matches("\\d")) {
                if (!inside) {
                    inside = true;
                    ++count;
                }
            } else {
                inside = false;
            }
        }
        return count;
    }
}
