/*
 * RecentImagesMenu.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.view;

import cellxgui.controller.CellXController;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class RecentImagesMenu extends JMenu {

    private final static int MAX_IMAGES = 10;
    private final JFrame parentFrame;
    private final CellXController control;

    public RecentImagesMenu(JFrame parent, CellXController control) {
        this.parentFrame = parent;
        this.setName("Recent control images");
        this.control = control;
    }

    public void registerControlImage(File f, String type) {

        if (this.getMenuComponentCount() == MAX_IMAGES) {
            final ControlImage del = getControlImage(0);
            try {
                del.file.delete();
            } catch (SecurityException e) {
                Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Cannot delete temporary file");
            }
            this.remove(del);
        }
        add(new ControlImage(f, type));
    }

    private ControlImage getControlImage(int i) {
        final Object obj = super.getItem(i);
        if (obj instanceof ControlImage) {
            return (ControlImage) obj;
        }
        return null;
    }

    public class ControlImage extends JMenuItem {

        private final File file;
        private final String type;
        private String dispName;
        private final int hour, min, sec;

        public ControlImage(final File file, final String type) {
            this.file = file;
            this.type = type;
            this.hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
            this.min = Calendar.getInstance().get(Calendar.MINUTE);
            this.sec = Calendar.getInstance().get(Calendar.SECOND);
            this.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {

                    control.loadAndShowControlImage(file);

                }
            });

            this.dispName = String.format("%s (%2d:%2d:%2d)", type, hour, min, sec);
            this.setToolTipText(file.toString());
            this.setText(dispName);
        }

        @Override
        public String toString() {
            return dispName;
        }
    }
}
