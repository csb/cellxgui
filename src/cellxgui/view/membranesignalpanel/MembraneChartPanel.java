
package cellxgui.view.membranesignalpanel;

import cellxgui.controller.CellXGuiConfiguration;
import cellxgui.controller.CellXParameter.PROPERTY_TYPE;
import cellxgui.model.signalaligner.XYMembraneProfile;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JOptionPane;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.title.TextTitle;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class MembraneChartPanel extends ChartPanel {

    private final MembraneLocationMarkerOverlay mlmo = new MembraneLocationMarkerOverlay();

    public MembraneChartPanel() {
        this(createChart(null));
    }

    public MembraneChartPanel(JFreeChart chart) {
        super(chart, false, true, true, true, true);
    }

    @Override
    public void paint(Graphics g) {      
        super.paint(g);
        if(getChart().getXYPlot().getDataset().getItemCount(0)!=0)
        mlmo.paintOverlay((Graphics2D) g, this);
    }

    public void updateDataSet(XYMembraneProfile profile) {
        this.setChart(createChart(profile));
        if(profile!=null){
            mlmo.setMembraneLocation(profile.getMembraneLocationEstimate());
            mlmo.setMembraneWidth(profile.getMembraneWidthEstimate());
        }
        this.updateUI();
    }

    public void setMembraneLocation(int loc) {
        mlmo.setMembraneLocation(loc);
    }

    public void setMembraneWidth(int w){
        mlmo.setMembraneWidth(w);
    }
    
    
    public static MembraneChartPanel createTestPanel() {

        JFreeChart chart = createChart(null);
        final MembraneChartPanel mcp = new MembraneChartPanel(chart);
        mcp.addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent e) {
                //final double x = Math.round(mcp.getChart().getXYPlot().getDomainAxis().java2DToValue(e.getX(), mcp.getScreenDataArea(), mcp.getChart().getXYPlot().getDomainAxisEdge()));

                //mcp.mlmo.setMembraneLocation(x);
                
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {

                if (e.getButton() == MouseEvent.BUTTON1) {
                    if( mcp.getChart().getXYPlot().getDataset()==null) return;
                    final NumberAxis xAxis = (NumberAxis) mcp.getChart().getXYPlot().getDomainAxis();
                    final double d = xAxis.getRange().getLength();

                    final Number minL = (Number) CellXGuiConfiguration.getInstance().membraneIntensityProfileParameter.get(PROPERTY_TYPE.MIN_ELEMENT_NUM);
                    int minENum = 1; 
                    if( minL!=null ) minENum = minL.intValue();
                    if (d < minENum ) {
                        JOptionPane.showMessageDialog(mcp, "Minimum length of the membrane profile is " +  minL.intValue());
                        mcp.restoreAutoBounds();
                    }
                    mcp.restoreAutoRangeBounds();
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });
        mcp.mlmo.setMembraneLocation(-100);
        return mcp;
    }

    private static JFreeChart createChart(XYMembraneProfile dataset) {
        JFreeChart chart = ChartFactory.createXYLineChart(
                "Membrane Profile", // chart title
                "Pixels", // x axis label
                "Intensity", // y axis label
                dataset, // data
                PlotOrientation.VERTICAL,
                false, // include legend
                true, // tooltips
                false // urls
                );
        chart.setTitle(new TextTitle("Membrane Profile", chart.getTitle().getFont().deriveFont(4)));
        chart.setBorderVisible(true);
        chart.setBackgroundPaint(Color.white);
        XYPlot plot = chart.getXYPlot();
        plot.setOutlineVisible(false);
        plot.setBackgroundPaint(null);
        // change the auto tick unit selection to integer units only...
        NumberAxis xAxis = (NumberAxis) plot.getDomainAxis();
        xAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        return chart;
    }
}
