/*
 * ZoomImagePanel.java
 *
 * Copyright (C) 2011 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.view.zoomimagepanel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class ZoomImagePanel extends JPanel {

    protected BufferedImage image;
    private double magnification = 1.0;
    int centerX, centerY;
    private int x1, x2, y1, y2;
    private Point2D mouse;
    private final MouseListener defaultMouseListener = getDefaultMouseListener();
    private final MouseMotionListener defaultMouseMotionListener = getDefaultMouseMotionListener();

    public ZoomImagePanel() {
        super();
        setImage(createBlackImage(this.getPreferredSize()));
    }

    public final void setImage(BufferedImage image) {
        this.image = image;
        x1 = 0;
        y1 = 0;
        x2 = image.getWidth();
        y2 = image.getHeight();
        centerX = image.getWidth() / 2;
        centerY = image.getHeight() / 2;
        magnification = 1.0;
        repaint();
    }

    public void setDefaultMouseListerenEnabled(boolean b) {
        if (b) {
            for (MouseListener ml : getMouseListeners()) {
                if (ml == defaultMouseListener) {
                    return;
                }
            }
            addMouseListener(defaultMouseListener);
        } else {
            removeMouseListener(defaultMouseListener);
        }
    }

    public void setDefaultMouseMotionListenerEnabled(boolean b) {
        if (b) {
            setDefaultMouseListerenEnabled(b);
            for (MouseMotionListener mml : getMouseMotionListeners()) {
                if (mml == defaultMouseMotionListener) {
                    return;
                }
            }
            addMouseMotionListener(defaultMouseMotionListener);
        } else {
            removeMouseMotionListener(defaultMouseMotionListener);
        }
    }

    public double getDisplayToImageRatio() {
        final double visiblePixelWidth = this.getWidth();
        final double sourcePixelWidth = x2 - x1 + 1;
        return visiblePixelWidth / sourcePixelWidth;
    }

    private MouseListener getDefaultMouseListener() {
        return new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent e) {
                //centerX = canvasX2image(e.getX());
                //centerY = canvasY2image(e.getY());
                setCenter(e.getX(), e.getY());
                if (e.getButton() == MouseEvent.BUTTON1 && !(e.isControlDown())) {
                    increaseMagnification();
                } else {
                    decreaseMagnification();
                }
                //System.out.printf("centerX=%d centerY=%d mag=%f\n", centerX, centerY, magnification);
                repaint();
            }

            @Override
            public void mousePressed(MouseEvent e) {
                mouse = e.getPoint();
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        };
    }

    private MouseMotionListener getDefaultMouseMotionListener() {
        return new MouseMotionListener() {

            @Override
            public void mouseDragged(MouseEvent e) {
                dragCenter((int) mouse.getX(), (int) mouse.getY(), e.getX(), e.getY());
                mouse = e.getPoint();
                repaint();
            }

            @Override
            public void mouseMoved(MouseEvent e) {
            }
        };
    }

    private void registerMouse() {
        this.addMouseListener(new MouseListener() {

            @Override
            public void mouseClicked(MouseEvent e) {
                //centerX = canvasX2image(e.getX());
                //centerY = canvasY2image(e.getY());
                setCenter(e.getX(), e.getY());
                if (e.getButton() == MouseEvent.BUTTON1) {
                    increaseMagnification();
                } else {
                    decreaseMagnification();
                }
                //System.out.printf("centerX=%d centerY=%d mag=%f\n", centerX, centerY, magnification);
                repaint();
            }

            @Override
            public void mousePressed(MouseEvent e) {
                mouse = e.getPoint();
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });


        this.addMouseMotionListener(new MouseMotionListener() {

            @Override
            public void mouseDragged(MouseEvent e) {
                dragCenter((int) mouse.getX(), (int) mouse.getY(), e.getX(), e.getY());
                mouse = e.getPoint();
                repaint();
            }

            @Override
            public void mouseMoved(MouseEvent e) {
            }
        });
    }

    @Override
    public void setSize(Dimension d) {
        setSize(d.width, d.height);
    }

    @Override
    public void setSize(int width, int height) {
        super.setSize(width,
                (int) ((width / (double) image.getWidth()) * image.getHeight()));
    }

    @Override
    public void setBounds(Rectangle r) {
        super.setBounds(r.x, r.y, r.width, r.height);
    }

    @Override
    public void setBounds(int x, int y, int width, int height) {
        super.setBounds(x, y, width, (int) ((width / (double) image.getWidth()) * image.getHeight()));
    }
    /*
    @Override
    public Dimension getPreferredSize() {
    if( this.getWidth()!=0 && this.getHeight()!=0 && image!=null){
    
    return getFixedAspectRatioDimension();
    }else
    return super.getPreferredSize();
    }
    
    @Override
    public Dimension getMinimumSize() {
    return super.getPreferredSize();
    }
    
     */

    public Rectangle getOptimalBoundsIn(int w, int h) {

        if (image == null) {
            return null;
        }

        if (image.getWidth() > image.getHeight()) {
            // enlarge according to the width
            final int ph = (int) ((w / (double) image.getWidth()) * image.getHeight());
            final int py = (int) Math.max(0, (h - ph) / 2);
            return new Rectangle(0, py, w, ph);
        } else {
            //enlarge according to height
            final int pw = (int) ((h / (double) image.getHeight()) * image.getWidth());
            final int px = (int) Math.max(0, (w - pw) / 2);
            return new Rectangle(px, 0, pw, h);
        }

    }

    public Rectangle getOptimalBounds(int maxElongation) {

        if (image == null) {
            return null;
        }

        if (image.getWidth() > image.getHeight()) {
            // enlarge according to the width
            final int ph = (int) ((maxElongation / (double) image.getWidth()) * image.getHeight());
            return new Rectangle(0, 0, maxElongation, ph);
        } else {
            //enlarge according to height
            final int pw = (int) ((maxElongation / (double) image.getHeight()) * image.getWidth());
            return new Rectangle(0, 0, pw, maxElongation);
        }

    }

    public Dimension getFixedAspectRatioDimension() {

        if (image == null) {
            return null;
        }

        final int maxElongation = this.getWidth();

        if (image.getWidth() > image.getHeight()) {
            // enlarge according to the width
            final int ph = (int) ((maxElongation / (double) image.getWidth()) * image.getHeight());
            return new Dimension(maxElongation, ph);
        } else {
            //enlarge according to height
            final int pw = (int) ((maxElongation / (double) image.getHeight()) * image.getWidth());
            return new Dimension(pw, maxElongation);
        }

    }

    @Override
    public void paint(Graphics g) {

        final int w = this.getWidth();
        final int h = this.getHeight();

        //System.out.printf("%d %d\n", w, h);

        if (magnification == 1.0) {
            g.drawImage(image, 0, 0, w, h, this);
            x1 = 0;
            y1 = 0;
            x2 = image.getWidth();
            y2 = image.getHeight();
        } else {

            final int dx = (int) (image.getWidth() / (2 * magnification));
            final int dy = (int) (image.getHeight() / (2 * magnification));

            //System.out.println("DX:" +dx);

            //System.out.println("DY:" +dy);

            //System.out.printf("centerX=%d centerY=%d\n", centerX, centerY);
            x1 = Math.max(0, centerX - dx);
            y1 = Math.max(0, centerY - dy);
            x2 = Math.min(image.getWidth(), centerX + dx);
            y2 = Math.min(image.getHeight(), centerY + dy);

            if (x2 - x1 < 2 * dx) {
                if (x1 == 0) {
                    x2 = x1 + 2 * dx;
                } else {
                    x1 = x2 - 2 * dx;
                }
            }


            if (y2 - y1 < 2 * dy) {
                if (y1 == 0) {
                    y2 = y1 + 2 * dy;
                } else {
                    y1 = y2 - 2 * dy;
                }

            }
            g.drawImage(image, 0, 0, w, h, x1, y1, x2, y2, this);
        }
        centerX = (x1 + x2) / 2;
        centerY = (y1 + y2) / 2;
        //   System.out.printf("Rectangle x1=%d y1=%d x2=%d y2=%d\n", x1, y1,x2,y2);
        //   System.out.printf("   w=%d h=%d aspect ratio=%f\n", x2-x1, y2-y1,  (x2-x1)/(double)(y2-y1));


        g.setColor(new Color(0, 200, 100, 50));
        g.drawRect(imageX2canvas(centerX), imageY2canvas(centerY), imageX2canvas(centerX + 1) - imageX2canvas(centerX), imageY2canvas(centerY + 1) - imageY2canvas(centerY));
    }

    public void increaseMagnification() {
        if (magnification < 32) {
            ++magnification;
        }
    }

    public void decreaseMagnification() {
        if (magnification >= 2) {
            --magnification;
        }
    }

    public void setMagnification(double d) {
        if (d < 1.0) {
            magnification = 1.0;
        } else if (d > 32.0) {
            magnification = 32.0;
        } else {
            magnification = d;
        }
        repaint();
    }

    public void setCenter(int canvasx, int canvasy) {
        centerX = canvasX2image(canvasx);
        centerY = canvasY2image(canvasy);
    }

    public void dragCenter(int oldX, int oldY, int newX, int newY) {
        centerX += canvasX2image(oldX) - canvasX2image(newX);
        centerY += canvasY2image(oldY) - canvasY2image(newY);
    }

    public Rectangle getCurrentSourceRectangle() {
        return new Rectangle(x1, y1, x2 - x1, y2 - y1);

    }

    public int canvasX2image(int x) {
        final double pixelXLength = this.getWidth() / (double) (x2 - x1);
        return (int) (x1 + x / pixelXLength);
    }

    public int canvasY2image(int y) {
        final double pixelYLength = this.getHeight() / (double) (y2 - y1);
        return (int) (y1 + y / pixelYLength);
    }

    public int imageX2canvas(int x) {
        final double pixelXLength = this.getWidth() / (double) (x2 - x1);
        return (int) ((x - x1) * pixelXLength);
    }

    public int imageY2canvas(int y) {
        final double pixelYLength = this.getHeight() / (double) (y2 - y1);
        return (int) ((y - y1) * pixelYLength);
    }

    private static BufferedImage createBlackImage(Dimension d) {
        final BufferedImage ret = new BufferedImage(d.width, d.height, BufferedImage.TYPE_INT_RGB);
        final Graphics2D g = (Graphics2D) ret.getGraphics();
        g.setColor(Color.DARK_GRAY);
        g.fillRect(0, 0, d.width, d.height);
        return ret;
    }

    public double getMagnification() {
        return magnification;
    }
}
