/*
 * ZoomImagePanelOverlay.java
 *
 * Copyright (C) 2011 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.view.zoomimagepanel;

import java.awt.Graphics2D;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public interface PaintableOverlay {

    public void paint(Graphics2D g2);
}
