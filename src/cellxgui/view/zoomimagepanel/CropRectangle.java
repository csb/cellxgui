/*
 * CropRectangle.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.view.zoomimagepanel;

import java.awt.Graphics2D;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class CropRectangle extends Line {

    public final int rx, ry, rw, rh;

    public CropRectangle(int x1, int y1, int x2, int y2) {
        super(x1, y1, x2, y2);
        if (x1 < x2) {
            rx = x1;
            rw = x2 - x1;
        } else {
            rx = x2;
            rw = x1 - x2;
        }
        if (y1 < y2) {
            ry = y1;
            rh = y2 - y1;
        } else {
            ry = y2;
            rh = y1 - y2;
        }
    }

    @Override
    public void paint(Graphics2D g2) {
        g2.drawRect(rx, ry, rw, rh);
    }
}
