/*
 * ControlImageLoader.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.workers;

import cellxgui.controller.CellXController;
import cellxgui.view.dialogs.WorkerDialog;
import ij.ImagePlus;
import java.io.File;
import java.util.concurrent.CancellationException;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class ControlImageLoader extends SwingWorker<ImagePlus, Object> {

    private final CellXController control;
    private final File imageFileName;

    public ControlImageLoader(File f, CellXController control) {
        this.imageFileName = f;
        this.control = control;
    }

    @Override
    protected ImagePlus doInBackground() throws Exception {
        final ImagePlus ip = new ImagePlus(imageFileName.toString());
        return ip;
    }

    @Override
    protected void done() {
        try {
            final ImagePlus ip = get();
            control.showControlImage(ip.getBufferedImage(), null);
            firePropertyChange(WorkerDialog.DONE, null, null);
        } catch (CancellationException ce) {
            //do nothing
        } catch (Exception e) {
            firePropertyChange(WorkerDialog.FAILED, null, null);
            JOptionPane.showMessageDialog(control.getView(),
                    String.format("Cannot load \n'%s'\n",
                    imageFileName.toString()),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }
}
