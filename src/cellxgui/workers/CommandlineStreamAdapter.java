/*
 * CommandlineStreamAdapter.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.workers;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public interface CommandlineStreamAdapter {

    public void handleStderr(String line);

    public void handleStdout(String line);
}
