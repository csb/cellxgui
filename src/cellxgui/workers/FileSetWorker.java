/*
 * FileSetWorker.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.workers;

import cellxgui.controller.CellXController;
import cellxgui.controller.CellXGuiProperties;
import cellxgui.controller.filehandling.CellXFileSet;
import cellxgui.controller.filehandling.CellXTimeSeries;
import cellxgui.controller.filehandling.CellXTimeSeriesGenerator;
import cellxgui.controller.filehandling.FileSetCollisionException;
import cellxgui.controller.filehandling.NoOutOfFocusImagesFoundException;
import cellxgui.model.BatchProcessModel;
import cellxgui.view.dialogs.WorkerDialog;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import javax.swing.SwingWorker;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class FileSetWorker extends SwingWorker< List<CellXTimeSeries>, String> {

    private final CellXGuiProperties properties;
    private final BatchProcessModel batchModel;
    private final CellXController control;
    private volatile Exception e;

    public FileSetWorker(CellXGuiProperties properties, BatchProcessModel batchModel, CellXController control) {
        this.properties = properties;
        this.batchModel = batchModel;
        this.control = control;
    }

    @Override
    protected List<CellXTimeSeries> doInBackground() throws Exception {
        try {
            final CellXTimeSeriesGenerator gen = new CellXTimeSeriesGenerator(
                    batchModel.getImageDir(),
                    batchModel.getBatchResultsDir(),
                    batchModel.getOOFPattern(),
                    batchModel.getFluoPatterns(),
                    batchModel.getFramePattern(),
                    batchModel.getFlatFieldPattern(),
                    batchModel.isTrackingEnabled());
            final List<CellXTimeSeries> ret = gen.getTimeSeries();
            return ret;
        } catch (Exception ex) {
            this.e = ex;
            throw (e);
        }
    }

    @Override
    protected void done() {
        try {
            firePropertyChange(WorkerDialog.DONE, null, null);
            List<CellXTimeSeries> timeSeries = get();
            saveBatchInProperties();
            batchModel.setFileSetSeries(timeSeries);
            control.showNotificationMessage(String.format("%d series", timeSeries.size()));
            return;
        } catch (InterruptedException ex) {
            firePropertyChange(WorkerDialog.FAILED, null, null);
            handleException();
        } catch (ExecutionException ex) {
            firePropertyChange(WorkerDialog.FAILED, null, null);
            handleException();
        } catch (CancellationException ex) {
            //canceling is ok
            firePropertyChange(WorkerDialog.DONE, null, null);
        } catch (Exception ex) {
            firePropertyChange(WorkerDialog.FAILED, null, null);
            handleException();
        }

    }

    private void handleException() {
        if (e instanceof FileSetCollisionException) {
            FileSetCollisionException exception = (FileSetCollisionException) e;
            final String msg = String.format("File set collision:\n %s\n %s\n", formatFileSet(exception.getExisting()), formatFileSet(exception.getOther()));
            control.showErrorMessage("Error", msg);
        } else if (e instanceof NoOutOfFocusImagesFoundException) {
            NoOutOfFocusImagesFoundException exception = (NoOutOfFocusImagesFoundException) e;
            final String msg = String.valueOf("No out of focus images found that match \n'" + exception.getOofPattern().toString() + "' and '" + exception.getFramePattern() + "'");
            control.showErrorMessage("Error", msg);
        } else {
            control.showErrorMessage("Error", "File set generation failed");
        }
    }

    private String formatFileSet(CellXFileSet fs) {
        final StringBuilder sb = new StringBuilder();
        sb.append("Frame ").append(fs.getFrameIdx()).append("\n");
        final String fn = fs.getOutOfFocusImage().toString();
        if (fn.length() > 80) {
            sb.append(" OOF: ....").append(fn.substring(fn.length() - 80)).append("\n");
        } else {
            sb.append(" OOF: ").append(fn).append("\n");
        }
        return sb.toString();
    }

    private void saveBatchInProperties() {
        properties.setBatchImageDir(batchModel.getImageDir());
        properties.setFramePattern(batchModel.getFramePattern());
        properties.setOOFPattern(batchModel.getOOFPattern());
        properties.setFluoPatterns(batchModel.getFluoPatterns());
        properties.setFlatFieldPattern(batchModel.getFlatFieldPattern());
        properties.setTrackingEnabled(batchModel.isTrackingEnabled());
    }
}
