package cellxgui.workers;

import cellxgui.controller.CellXController;
import cellxgui.controller.CellXGuiConfiguration;
import cellxgui.model.CellXModel;
import cellxgui.view.dialogs.WorkerDialog;
import ij.ImagePlus;
import java.beans.PropertyChangeEvent;
import java.util.List;
import java.util.concurrent.CancellationException;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;
import mpicbg.ij.clahe.FastFlat;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class ClaheImageWorker extends SwingWorker<CellXModel, String> {

    private final CellXController control;

    public ClaheImageWorker(CellXController control) {
        this.control = control;
    }

    @Override
    protected CellXModel doInBackground() throws Exception {

        final ImagePlus ip = control.getImagePlus().duplicate();
        if (control.getModel().isClaheEnabled()) {
            publish("Adaptive Histogram Equalization");

            final int blockSizeIdx = CellXGuiConfiguration.getInstance().claheBlockSize.getLocation();

            final int blockRadius = (Integer) control.getModel().getAdvancedParameterValues()[blockSizeIdx] / 2;


            final int clipLimitIdx = CellXGuiConfiguration.getInstance().claheClipLimit.getLocation();

            final double clip = (Double) control.getModel().getAdvancedParameterValues()[clipLimitIdx];

            FastFlat.getInstance().run(ip, blockRadius, CellXGuiConfiguration.CLAHE_BINS, (float) clip, null, false);
        }
        final CellXModel ret = new CellXModel();
        publish("Setting new image data");
        ret.setImage(ip, control.getCurrentImage());
        publish("Transferring parameters");
        ret.setParameterValues(control.getModel().getParameterValues());
        ret.setAdvancedParameterValues(control.getModel().getAdvancedParameterValues());
        publish("Transferring shape data");
        ret.transferModelData(control.getModel());
        return ret;
    }

    @Override
    protected void done() {
        try {
            final CellXModel m = get();
            control.setModel(m);
            firePropertyChange(WorkerDialog.DONE, null, null);
            String msg;
            if (control.getModel().isClaheEnabled()) {
                msg = "Switched mode (Contrast Limited Adaptive Histogram Equalization)";
            } else {
                msg = "Switched mode (using flat image)";
            }
            control.propertyChange(new PropertyChangeEvent(this, CellXController.INFO_MESSAGE, null, String.valueOf(msg)));
        } catch (CancellationException ce) {
            revert();
        } catch (Exception e) {
            revert();
            firePropertyChange(WorkerDialog.FAILED, null, null);
            JOptionPane.showMessageDialog(control.getView(),
                    "There was an error:\n" + e.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    protected void process(List<String> chunks) {
        for (String msg : chunks) {
            firePropertyChange(WorkerDialog.STATUS_MSG, null, msg);
        }
    }

    private void revert() {
        final Boolean v = control.getModel().isClaheEnabled();
        control.setParameter(!v, CellXGuiConfiguration.getInstance().isGraphCutOnCLAHE.getLocation());
    }
}
