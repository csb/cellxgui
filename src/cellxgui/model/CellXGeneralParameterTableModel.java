/*
 * CellXGeneralParameterTableModel.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.model;

import cellxgui.controller.CellXParameter;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class CellXGeneralParameterTableModel extends AbstractTableModel {

    private final List<CellXParameter> params;
    private final Object[] values;

    public CellXGeneralParameterTableModel(List<CellXParameter> params) {
        this.params = params;
        this.values = new Object[params.size()];
        int i = 0;
        for (CellXParameter cxp : params) {
            values[i++] = cxp.getDefaultValue();
        }
    }

    @Override
    public String getColumnName(int column) {
        if (column == 0) {
            return "Parameter";
        } else if (column == 1) {
            return "Value";
        } else {
            return "";
        }
    }

    @Override
    public int getRowCount() {
        return values.length;
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (columnIndex == 0) {
            return params.get(rowIndex).getDisplayName();
        } else if (columnIndex == 1) {
            return values[rowIndex];
        }
        return null;
    }

    @Override
    public boolean isCellEditable(int row, int col) {
        return (col > 0);
    }

    @Override
    public void setValueAt(Object value, int row, int col) {
        if (col == 1) {
            values[row] = value;
            fireTableCellUpdated(row, col);
        }
    }

    public void setValue(Object value, int row) {
        setValueAt(value, row, 1);
    }

    public CellXParameter getParameterByRowIndex(int row) {
        return params.get(row);
    }

    public Object[] getValues() {
        return values;
    }

    void setValues(Object[] parameterValues) {
        for (int i = 0; i < parameterValues.length; ++i) {
            values[i] = parameterValues[i];
        }
    }
}
