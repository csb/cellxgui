/*
 * CellLengthMode.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.model.modes;

import cellxgui.controller.CellXGuiConfiguration.Mode;
import cellxgui.model.shapes.CellLength;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class CellLengthMode extends AbstractMode<CellLength> {

    private final Mode mode;
    private int maxLength;

    public CellLengthMode(Mode mode) {
        super();
        this.mode = mode;
        maxLength = 0;
    }

    @Override
    public void addShape(CellLength s) {
        shapes.add(s);
        maxLength = Math.max(maxLength, s.getPixelLength());
    }

    @Override
    public void deleteSelectedShapes(int[] indices) {
        super.deleteSelectedShapes(indices);
        maxLength = 0;
        for (CellLength s : shapes) {
            maxLength = Math.max(maxLength, s.getPixelLength());
        }
    }

    public int getMaxLength() {
        return maxLength;
    }
}
