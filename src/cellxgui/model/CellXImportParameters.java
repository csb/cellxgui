/*
 * CellXImportParameters.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.model;

import cellxgui.controller.CellXController;
import cellxgui.controller.CellXGuiConfiguration;
import cellxgui.controller.CellXParameter;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author chrimson
 */
public class CellXImportParameters {

    public static void importCellXParameters(CellXController control, File configfile) throws ParserConfigurationException, SAXException, IOException {

        final DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
        final DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
        final Document doc = docBuilder.parse(configfile);
        doc.getDocumentElement().normalize();
        Element root = doc.getDocumentElement();
        final NodeList list = root.getElementsByTagName(CellXGuiConfiguration.CellXConfigurationXMLParamNodeTagName);
        for (int i = 0; i < list.getLength(); ++i) {
            final Element paramNode = (Element) list.item(i);
            final String paramName = paramNode.getAttribute(CellXGuiConfiguration.CellXConfigurationXMLAttrName);
            if (!CellXGuiConfiguration.getInstance().hasParam(paramName)) {
                continue;
            }

            final CellXParameter cxp = CellXGuiConfiguration.getInstance().getParam(paramName);
            if (paramNode.hasAttribute(CellXGuiConfiguration.CellXConfigurationXMLAttrValue)) {
                final String value = paramNode.getAttribute(CellXGuiConfiguration.CellXConfigurationXMLAttrValue);
                final Object obj = convertNumber(value, cxp);
                if (obj != null) {
                    if (CellXGuiConfiguration.getInstance().isGeneralParameter(paramName)) {
                        control.setParameter(obj, cxp.getLocation());
                    } else {
                        control.setAdvancedParameter(obj, cxp.getLocation());
                    }
                }
            } else {
                final NodeList arrayElements = paramNode.getElementsByTagName(CellXGuiConfiguration.CellXConfigurationXMLArrayElementNodeTagName);
                final Number[] array = convertToArray(arrayElements, cxp);
                if( array!=null ){
                    if (CellXGuiConfiguration.getInstance().isGeneralParameter(paramName)) {
                        control.setParameter(array, cxp.getLocation());
                    } else {
                        control.setAdvancedParameter(array, cxp.getLocation());
                    }
                }else{
                      Logger.getLogger(CellXImportParameters.class.getName()).log(Level.WARNING, "Cannot convert array");
                }

            }
        }

    }

    private static Object convertNumber(String value, CellXParameter cxp) {
        Object obj = null;
        try {
            if (cxp.isDouble() || cxp.isDoubleArray()) {
                obj = Double.parseDouble(value);
            } else if (cxp.isInteger() || cxp.isIntegerArray()) {
                obj = Integer.parseInt(value);
            } else if (cxp.isBoolean()) {
                obj = value.equals("1") ? Boolean.TRUE : Boolean.FALSE;
            }
        } catch (NumberFormatException e) {
            Logger.getLogger(CellXImportParameters.class.getName()).log(Level.WARNING, "Cannot convert parameter value", e);
        }
        return obj;
    }

    private static Number[] convertToArray(NodeList arrayElements, CellXParameter cxp) {
        final ArrayList<Number> numbers = new ArrayList<Number>();

        for (int j = 0; j < arrayElements.getLength(); ++j) {
            final Element arrayElement = (Element) arrayElements.item(j);
            
            if (!arrayElement.hasAttribute(CellXGuiConfiguration.CellXConfigurationXMLAttrValue)) {
                return null;
            }
            final Object n = convertNumber(
                    arrayElement.getAttribute(CellXGuiConfiguration.CellXConfigurationXMLAttrValue),
                    cxp);
            if (n instanceof Number) {
                numbers.add((Number) n);
            } else {
                return null;
            }

        }

        final int N = numbers.size();
        if (cxp.isDoubleArray()) {
            final Double[] ret = new Double[N];
            for (int i = 0; i < N; ++i) {
                ret[i] = numbers.get(i).doubleValue();
            }
            return ret;
        } else if (cxp.isIntegerArray()) {
            final Integer[] ret = new Integer[N];
            for (int i = 0; i < N; ++i) {
                ret[i] = numbers.get(i).intValue();
            }
            return ret;
        }
        return null;
    }
}
