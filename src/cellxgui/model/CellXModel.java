/*
 * CellXModel.java
 *
 * Copyright (C) 2011 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.model;

import cellxgui.controller.CellXController;
import cellxgui.controller.CellXGuiConfiguration;
import cellxgui.controller.CellXGuiConfiguration.Mode;
import cellxgui.model.modes.*;
import cellxgui.model.shapes.*;
import cellxgui.model.signalaligner.XYMembraneProfile;
import ij.ImagePlus;
import ij.process.ColorProcessor;
import ij.process.ImageProcessor;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class CellXModel extends AbstractModel {

    private static final long serialVersionUID = 1L;
    private Mode currentImagePanelMode;
    private ImageProcessor imageProcessor;
    private BufferedImage image;
    private MembraneProfileMode membraneProfileMode = new MembraneProfileMode(Mode.MEMBRANE_PROFILE);
    private SeedSizeMode seedSizeMode = new SeedSizeMode(Mode.SEED_SIZE);
    private CellLengthMode cellLengthMode = new CellLengthMode(Mode.CELL_LENGTH);
    private CropMode cropMode = new CropMode(Mode.CROP);
    private ZoomMode zoomMode = new ZoomMode(Mode.ZOOM_PAN);
    private List<AbstractMode<? extends SelectableShape>> modes;
    private CellXGeneralParameterTableModel paramTableModel = new CellXGeneralParameterTableModel(CellXGuiConfiguration.getParameters());
    private CellXGeneralParameterTableModel advancedParamTableModel = new CellXGeneralParameterTableModel(CellXGuiConfiguration.getAdvancedParameters());
    private CellXController control;
    private File currentImage;
    private File flatFieldTestImage;
    private File fluoTestImage;

    public CellXModel() {
        this.image = createEmptyImage(600, 500);
        this.imageProcessor = new ColorProcessor(image).convertToShort(true);
        this.currentImagePanelMode = Mode.ZOOM_PAN;
        init();
    }

    private void init() {
        membraneProfileMode = new MembraneProfileMode(Mode.MEMBRANE_PROFILE);
        seedSizeMode = new SeedSizeMode(Mode.SEED_SIZE);
        cellLengthMode = new CellLengthMode(Mode.CELL_LENGTH);
        cropMode = new CropMode(Mode.CROP);
        zoomMode = new ZoomMode(Mode.ZOOM_PAN);
        paramTableModel = new CellXGeneralParameterTableModel(CellXGuiConfiguration.getParameters());
        advancedParamTableModel = new CellXGeneralParameterTableModel(CellXGuiConfiguration.getAdvancedParameters());
        this.modes = new ArrayList<AbstractMode<? extends SelectableShape>>(5);
        for (int i = 0; i < Mode.values().length; ++i) {
            if (i == Mode.ZOOM_PAN.ordinal()) {
                this.modes.add(zoomMode);
            } else if (i == Mode.CELL_LENGTH.ordinal()) {
                this.modes.add(cellLengthMode);
            } else if (i == Mode.MEMBRANE_PROFILE.ordinal()) {
                this.modes.add(membraneProfileMode);
            } else if (i == Mode.CROP.ordinal()) {
                this.modes.add(cropMode);
            } else if (i == Mode.SEED_SIZE.ordinal()) {
                this.modes.add(seedSizeMode);
            }
        }
        this.modes.set(Mode.ZOOM_PAN.ordinal(), zoomMode);
        this.modes.set(Mode.MEMBRANE_PROFILE.ordinal(), membraneProfileMode);
        this.modes.set(Mode.CELL_LENGTH.ordinal(), cellLengthMode);
        this.modes.set(Mode.SEED_SIZE.ordinal(), seedSizeMode);
        this.modes.set(Mode.CROP.ordinal(), cropMode);
    }

    public CellXModel(CellXController control) {
        this();
        this.setController(control);
    }

    public final void setController(CellXController control) {
        this.control = control;
        this.addPropertyChangeListener(control);
        initTableListeners();
    }

    private void initTableListeners() {
        this.paramTableModel.addTableModelListener(new TableModelListener() {

            @Override
            public void tableChanged(TableModelEvent e) {
                control.setModified();
                if (e.getFirstRow() == CellXGuiConfiguration.getInstance().membraneIntensityProfileParameter.getLocation()) {
                    final Double[] newValue = (Double[]) paramTableModel.getValueAt(e.getFirstRow(), 1);
                    control.setMembraneProfile(newValue);
                } else if (e.getFirstRow() == CellXGuiConfiguration.getInstance().membraneLocationParameter.getLocation()) {
                    final Integer newValue = (Integer) paramTableModel.getValueAt(e.getFirstRow(), 1);
                    if (newValue != null) {
                        firePropertyChange(CellXController.MEMBRANE_LOCATION, null, newValue);
                    }
                } else if (e.getFirstRow() == CellXGuiConfiguration.getInstance().membraneWidthParameter.getLocation()) {
                    final Integer newValue = (Integer) paramTableModel.getValueAt(e.getFirstRow(), 1);
                    if (newValue != null) {
                        firePropertyChange(CellXController.MEMBRANE_WIDTH, null, newValue);
                    }
                } else if (e.getFirstRow() == CellXGuiConfiguration.getInstance().cropRegionBoundaryParameter.getLocation()) {
                    final Number[] newValue = (Number[]) paramTableModel.getValueAt(e.getFirstRow(), 1);
                    if (newValue != null) {
                        final Coordinates coords = new Coordinates(
                                newValue[0].intValue(),
                                newValue[1].intValue(),
                                newValue[0].intValue() + newValue[2].intValue(),
                                newValue[1].intValue() + newValue[3].intValue());
                        cropMode.addShape(new CropRegion(coords, Mode.CROP.color, CellXGuiConfiguration.selectionColor));
                        firePropertyChange(CellXController.LIST_BOX_MODEL, null, cropMode.getShapeList());
                    } else {
                        cropMode.getShapeList().clear();
                        firePropertyChange(CellXController.LIST_BOX_MODEL, null, cropMode.getShapeList());
                    }
                }
            }
        });

        this.advancedParamTableModel.addTableModelListener(new TableModelListener() {

            @Override
            public void tableChanged(TableModelEvent e) {
                control.setModified();
                if (e.getFirstRow() == CellXGuiConfiguration.getInstance().claheClipLimit.getLocation()) {
                    control.updateCLAHEMode();
                } else if (e.getFirstRow() == CellXGuiConfiguration.getInstance().claheBlockSize.getLocation()) {
                    control.updateCLAHEMode();
                } else if (e.getFirstRow() == CellXGuiConfiguration.getInstance().isGraphCutOnCLAHE.getLocation()) {
                    control.updateCLAHEMode();
                }
            }
        });
    }

    public void fireAllProperties() {
        firePropertyChange(CellXController.IMAGE_PROPERTY, null, image);
        firePropertyChange(CellXController.PARAM_TABLE_MODEL_CHANGE, null, paramTableModel);
        firePropertyChange(CellXController.ADV_PARAM_TABLE_MODEL_CHANGE, null, advancedParamTableModel);

        firePropertyChange(CellXController.IMAGE_PANEL_MODE, null, currentImagePanelMode);
        firePropertyChange(CellXController.OVERLAY_CHANGE, null, null);
        firePropertyChange(CellXController.LIST_BOX_MODEL, null, modes.get(currentImagePanelMode.ordinal()).getShapeList());

        final Double[] memProfile = getCurrentMembraneProfile();

        if (memProfile != null) {
            firePropertyChange(CellXController.MEMBRANE_PROFILE, null, new XYMembraneProfile(memProfile));
        } else {
            firePropertyChange(CellXController.MEMBRANE_PROFILE, null, new XYMembraneProfile());
        }

        final Integer memLocation = getCurrentMembraneLocation();
        if (memLocation != null) {
            firePropertyChange(CellXController.MEMBRANE_LOCATION, null, memLocation);
        }

        final Integer memWidth = getCurrentMembraneWidth();
        if (memWidth != null) {
            firePropertyChange(CellXController.MEMBRANE_WIDTH, null, memWidth);
        }

        if (fluoTestImage != null) {
            firePropertyChange(CellXController.FLUO_IMAGE_CHANGED, null, this.fluoTestImage.toString());
        }

        if (flatFieldTestImage != null) {
            firePropertyChange(CellXController.FLAT_FIELD_IMAGE_CHANGED, null, this.flatFieldTestImage.toString());
        }
    }

    public void setImage(ImagePlus ip, File f) {
        //System.out.println("MODEL: Image changed");
        this.currentImage = f;
        final BufferedImage oldValue = this.image;
        this.image = ip.getBufferedImage();
        this.imageProcessor = ip.getProcessor();
        firePropertyChange(CellXController.IMAGE_PROPERTY, oldValue, image);
        final String msg = String.format("Loaded image '%s' width=%d  height=%d  bit-depth=%d", ip.getTitle(), ip.getWidth(), ip.getHeight(), ip.getBitDepth());
        firePropertyChange(CellXController.INFO_MESSAGE, null, msg);

        CellXGuiConfiguration.getInstance().claheBlockSize.setMaximum(getMinimumImageDim() / 2);
        updateAdvancedParameter(
                CellXGuiConfiguration.getInstance().claheBlockSize.getDefaultValue(),
                CellXGuiConfiguration.getInstance().claheBlockSize.getLocation());

    }

    public void setSelectedShapes(int[] indices) {
        if (currentImagePanelMode != Mode.ZOOM_PAN) {
            modes.get(currentImagePanelMode.ordinal()).setSelectedShapes(indices);
            firePropertyChange(CellXController.OVERLAY_CHANGE, null, null);
        }
    }

    public void setFluoTestImage(File fluoTestImage) {
        firePropertyChange(CellXController.FLUO_IMAGE_CHANGED, this.fluoTestImage == null ? "" : this.fluoTestImage.toString(), fluoTestImage == null ? "" : fluoTestImage.toString());
        this.fluoTestImage = fluoTestImage;
    }

    public void setFlatFieldTestImage(File flatFieldTestImage) {
        firePropertyChange(CellXController.FLAT_FIELD_IMAGE_CHANGED, this.flatFieldTestImage == null ? "" : this.flatFieldTestImage.toString(), flatFieldTestImage == null ? "" : flatFieldTestImage.toString());
        this.flatFieldTestImage = flatFieldTestImage;
    }

    public void setCurrentImage(File currentImage) {
        this.currentImage = currentImage;
    }

    public void deleteSelectedShapes(int[] indices) {
        modes.get(currentImagePanelMode.ordinal()).deleteSelectedShapes(indices);
        firePropertyChange(CellXController.LIST_BOX_MODEL, null, modes.get(currentImagePanelMode.ordinal()).getShapeList());
        if (currentImagePanelMode == Mode.MEMBRANE_PROFILE) {
            if (membraneProfileMode.isEmpty()) {
                updateParameter(CellXGuiConfiguration.getInstance().membraneWidthParameter.getDefaultValue(), CellXGuiConfiguration.getInstance().membraneWidthParameter.getLocation());
                updateParameter(CellXGuiConfiguration.getInstance().membraneLocationParameter.getDefaultValue(), CellXGuiConfiguration.getInstance().membraneLocationParameter.getLocation());
                updateParameter(CellXGuiConfiguration.getInstance().membraneIntensityProfileParameter.getDefaultValue(), CellXGuiConfiguration.getInstance().membraneIntensityProfileParameter.getLocation());
            } else {
                autoMembraneIntensityProfile();
            }
        } else if (currentImagePanelMode == Mode.CELL_LENGTH) {
            if (cellLengthMode.isEmpty()) {
                updateParameter(CellXGuiConfiguration.getInstance().maximumCellLengthParameter.getDefaultValue(), CellXGuiConfiguration.getInstance().maximumCellLengthParameter.getLocation());
            } else {
                autoMaximumCellLength();
            }
        } else if (currentImagePanelMode == Mode.CROP) {
            if (cropMode.isEmpty()) {
                updateParameter(CellXGuiConfiguration.getInstance().cropRegionBoundaryParameter.getDefaultValue(), CellXGuiConfiguration.getInstance().cropRegionBoundaryParameter.getLocation());
            } else {
                autoCropRegion();
            }
        } else if (currentImagePanelMode == Mode.SEED_SIZE) {
            if (seedSizeMode.isEmpty()) {
                updateParameter(CellXGuiConfiguration.getInstance().seedRadiusLimitParameter.getDefaultValue(), CellXGuiConfiguration.getInstance().seedRadiusLimitParameter.getLocation());
            } else {
                autoSeedRadiusLimit();
            }
        }
        //repaint overlay
        firePropertyChange(CellXController.OVERLAY_CHANGE, null, null);
    }

    public void addMembraneProfile(Coordinates coords) {
        final MembraneProfile mp = new MembraneProfile(coords, Mode.MEMBRANE_PROFILE.color, CellXGuiConfiguration.selectionColor, imageProcessor);
        membraneProfileMode.addShape(mp);
        firePropertyChange(CellXController.LIST_BOX_MODEL, null, membraneProfileMode.getShapeList());
        autoMembraneIntensityProfile();
    }

    public void transferModelData(CellXModel srcModel) {

        setFluoTestImage(srcModel.getFluoTestImage());
        setFlatFieldTestImage(srcModel.getFlatFieldTestImage());

        for (MembraneProfile src : srcModel.membraneProfileMode.getShapeList()) {
            final MembraneProfile dst = new MembraneProfile(src.getCoordinates(), Mode.MEMBRANE_PROFILE.color, CellXGuiConfiguration.selectionColor, imageProcessor);
            dst.setSelected(src.isSelected());
            membraneProfileMode.addShape(dst);
        }
        autoMembraneIntensityProfile();
        autoMembraneLocation();

        if (srcModel.getCurrentMembraneWidth() == null) {
            autoMembraneWidth();
        } else {
            paramTableModel.setValue(srcModel.getCurrentMembraneWidth(), CellXGuiConfiguration.getInstance().membraneWidthParameter.getLocation());
        }

        for (CellLength src : srcModel.cellLengthMode.getShapeList()) {
            final CellLength dst = new CellLength(src.getCoordinates(), Mode.CELL_LENGTH.color, CellXGuiConfiguration.selectionColor);
            dst.setSelected(src.isSelected());
            cellLengthMode.addShape(dst);
        }

        for (SeedSize srcS : srcModel.seedSizeMode.getShapeList()) {
            final SeedSize dst = new SeedSize(srcS.getCoordinates(), Mode.SEED_SIZE.color, CellXGuiConfiguration.selectionColor);
            dst.setSelected(srcS.isSelected());
            seedSizeMode.addShape(dst);
        }

        for (CropRegion srcCR : srcModel.cropMode.getShapeList()) {
            final CropRegion dst = new CropRegion(srcCR.getCoordinates(), Mode.CROP.color, CellXGuiConfiguration.selectionColor);
            dst.setSelected(srcCR.isSelected());
            cropMode.addShape(dst);
        }

        this.currentImagePanelMode = srcModel.currentImagePanelMode;
    }

    public void addSeedSize(Coordinates coords) {
        seedSizeMode.addShape(new SeedSize(coords, Mode.SEED_SIZE.color, CellXGuiConfiguration.selectionColor));
        firePropertyChange(CellXController.OVERLAY_CHANGE, null, null);
        firePropertyChange(CellXController.LIST_BOX_MODEL, null, seedSizeMode.getShapeList());
        autoSeedRadiusLimit();
    }

    public void addCellLength(Coordinates coords) {
        cellLengthMode.addShape(new CellLength(coords, Mode.CELL_LENGTH.color, CellXGuiConfiguration.selectionColor));
        firePropertyChange(CellXController.OVERLAY_CHANGE, null, null);
        firePropertyChange(CellXController.LIST_BOX_MODEL, null, cellLengthMode.getShapeList());
        autoMaximumCellLength();
    }

    public void setCrop(Coordinates coords) {
        cropMode.addShape(new CropRegion(coords, Mode.CROP.color, CellXGuiConfiguration.selectionColor));
        firePropertyChange(CellXController.LIST_BOX_MODEL, null, cropMode.getShapeList());
        autoCropRegion();
    }

    public void setImagePanelMode(Mode mode) {
        //System.out.println("MODEL: setting image panel mode to " + mode);
        final Mode old = currentImagePanelMode;
        currentImagePanelMode = mode;
        firePropertyChange(CellXController.IMAGE_PANEL_MODE, old, currentImagePanelMode);
        firePropertyChange(CellXController.OVERLAY_CHANGE, null, null);
        if (mode != Mode.ZOOM_PAN) {
            firePropertyChange(CellXController.LIST_BOX_MODEL, null, modes.get(currentImagePanelMode.ordinal()).getShapeList());
        }
    }

    public void setMembraneProfile(Double[] newValue) {
        if (newValue != null) {
            final XYMembraneProfile profile = new XYMembraneProfile(newValue);
            firePropertyChange(CellXController.MEMBRANE_PROFILE, null, profile);
            paramTableModel.setValue(
                    profile.getMembraneLocationEstimate(),
                    CellXGuiConfiguration.getInstance().membraneLocationParameter.getLocation());

            if (getCurrentMembraneWidth() == null) {
                paramTableModel.setValue(
                        profile.getMembraneWidthEstimate(),
                        CellXGuiConfiguration.getInstance().membraneWidthParameter.getLocation());
            }
        } else {
            firePropertyChange(CellXController.MEMBRANE_PROFILE, null, new XYMembraneProfile(new Double[]{}));
            updateParameter(CellXGuiConfiguration.getInstance().membraneWidthParameter.getDefaultValue(), CellXGuiConfiguration.getInstance().membraneWidthParameter.getLocation());
            updateParameter(CellXGuiConfiguration.getInstance().membraneLocationParameter.getDefaultValue(), CellXGuiConfiguration.getInstance().membraneLocationParameter.getLocation());
        }
    }

    private static BufferedImage createEmptyImage(int w, int h) {
        final BufferedImage ret = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
        final Graphics2D g = (Graphics2D) ret.getGraphics();
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        // g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, w, h);
        g.setColor(Color.GREEN);

        final String text = "CellX";
        g.setFont(new Font(Font.SERIF, Font.ITALIC, 40));

        int xpos = w / 2 - g.getFontMetrics().stringWidth(text);
        for (int i = 0; i < text.length(); ++i) {
            final int length = g.getFontMetrics().stringWidth(text.substring(i, i + 1));
            g.drawString(text.substring(i, i + 1), xpos, h / 2);
            xpos += length + 15;
        }
        g.setColor(Color.GREEN);
        g.setFont(new Font(Font.DIALOG, Font.PLAIN, 9));
        final String v = String.format("version %d.%02d", CellXGuiConfiguration.version, CellXGuiConfiguration.release);
        final int vlength = g.getFontMetrics().stringWidth(v);
        g.drawString(v, w - vlength - 4, h - 4);
        return ret;
    }

    public void updateAdvancedParameter(Object value, int row) {
        advancedParamTableModel.setValueAt(value, row, 1);
    }

    public void updateParameter(Object value, int row) {
        paramTableModel.setValueAt(value, row, 1);
    }

    public void autoCropRegion() {
        if (cropMode.isEmpty()) {
            firePropertyChange(CellXController.INFO_MESSAGE, null, "No crop region selected");
        } else {
            paramTableModel.setValue(
                    cropMode.getShapeList().get(0).getCropRegion(),
                    CellXGuiConfiguration.getInstance().cropRegionBoundaryParameter.getLocation());
        }
    }

    public void autoSeedRadiusLimit() {
        if (seedSizeMode.isEmpty()) {
            firePropertyChange(CellXController.INFO_MESSAGE, null, "No seed radii specified yet");
        } else {
            paramTableModel.setValue(
                    seedSizeMode.getMinMaxRadius(),
                    CellXGuiConfiguration.getInstance().seedRadiusLimitParameter.getLocation());
        }
    }

    public void autoMembraneWidth() {
        final Double[] currentProfile = getCurrentMembraneProfile();
        if (currentProfile == null) {
            firePropertyChange(CellXController.INFO_MESSAGE, null, "No profile available");
        } else {
            final XYMembraneProfile tmp = new XYMembraneProfile(currentProfile);
            paramTableModel.setValue(
                    tmp.getMembraneWidthEstimate(),
                    CellXGuiConfiguration.getInstance().membraneWidthParameter.getLocation());
        }
    }

    public void autoMembraneLocation() {
        final Double[] currentProfile = getCurrentMembraneProfile();
        if (currentProfile == null) {
            firePropertyChange(CellXController.INFO_MESSAGE, null, "No profile available");
        } else {
            final XYMembraneProfile tmp = new XYMembraneProfile(currentProfile);
            paramTableModel.setValue(
                    tmp.getMembraneLocationEstimate(),
                    CellXGuiConfiguration.getInstance().membraneLocationParameter.getLocation());
        }
    }

    public void autoMembraneIntensityProfile() {
        if (membraneProfileMode.isEmpty()) {
            firePropertyChange(CellXController.INFO_MESSAGE, null, "No profiles available");
            setMembraneProfile(null);

        } else {
            membraneProfileMode.getSignalAligner().update();
            final Double[] newSignal = membraneProfileMode.getSignalAligner().getTrimmedSignal();
            setMembraneProfile(newSignal);
            paramTableModel.setValue(
                    newSignal,
                    CellXGuiConfiguration.getInstance().membraneIntensityProfileParameter.getLocation());
        }
    }

    public void autoMaximumCellLength() {
        if (cellLengthMode.isEmpty()) {
            firePropertyChange(CellXController.INFO_MESSAGE, null, "No cell length specified yet");
        } else {
            paramTableModel.setValue(
                    cellLengthMode.getMaxLength(),
                    CellXGuiConfiguration.getInstance().maximumCellLengthParameter.getLocation());
        }
    }

    public void updateMembraneLocation(int pos) {
        paramTableModel.setValue(
                pos,
                CellXGuiConfiguration.getInstance().membraneLocationParameter.getLocation());
        firePropertyChange(CellXController.MEMBRANE_LOCATION, null, pos);
    }

    public void updateMembraneWidth(int width) {
        paramTableModel.setValue(
                width,
                CellXGuiConfiguration.getInstance().membraneWidthParameter.getLocation());
        firePropertyChange(CellXController.MEMBRANE_WIDTH, null, width);
    }

    public void updateMembraneProfileRegion(int beginIdx, int endIdx) {


        final Double[] currentSignal = getCurrentMembraneProfile();

        if (currentSignal == null) {
            return;
        }
        endIdx = Math.min(endIdx, currentSignal.length);
        if (beginIdx < 0
                || beginIdx >= currentSignal.length - 1
                || endIdx < 0
                || endIdx < beginIdx) {
            firePropertyChange(CellXController.INFO_MESSAGE, null, "Invalid profile region");
        } else {
            //System.out.println("Updating profile region: " + beginIdx + " " + endIdx);
            // membraneProfileMode.getSignalAligner().updateToSubRegion(beginIdx, endIdx);   
            final Double[] newSignal = Arrays.copyOfRange(currentSignal, beginIdx, endIdx);
            setMembraneProfile(newSignal);
            paramTableModel.setValue(
                    newSignal,
                    CellXGuiConfiguration.getInstance().membraneIntensityProfileParameter.getLocation());
        }
    }

    public void resetMembraneProfilePanelData() {
        autoMembraneIntensityProfile();
    }

    public Object[] getParameterValues() {
        return paramTableModel.getValues();
    }

    public Object[] getAdvancedParameterValues() {
        return advancedParamTableModel.getValues();
    }

    public void setParameterValues(Object[] parameterValues) {
        paramTableModel.setValues(parameterValues);
    }

    public void setAdvancedParameterValues(Object[] parameterValues) {
        advancedParamTableModel.setValues(parameterValues);
    }

    public int getLengthOfMembraneProfile() {
        final Number[] n = getCurrentMembraneProfile();
        return n == null ? 0 : n.length;
    }

    public Boolean isClaheEnabled() {
        final int claheLoc = CellXGuiConfiguration.getInstance().isGraphCutOnCLAHE.getLocation();
        final Object claheValue = advancedParamTableModel.getValues()[claheLoc];
        if (claheValue != null && claheValue instanceof Boolean) {
            final Boolean b = (Boolean) claheValue;
            return b;
        }
        return false;
    }

    public static ImagePlus loadImageAndConvertToGrayScale(File imageFileName) {
        final ImagePlus ip = new ImagePlus(imageFileName.toString());
        if (ip.getType() == ImagePlus.COLOR_256 || ip.getType() == ImagePlus.COLOR_RGB) {
            ip.setProcessor(ip.getProcessor().convertToShort(true));
        }
        return ip;
    }

    public List<Coordinates> getMembraneProfileCoordinates() {
        return extractCoordinates(membraneProfileMode.getShapeList());
    }

    public List<Coordinates> getSeedSizeCoordinates() {
        return extractCoordinates(seedSizeMode.getShapeList());
    }

    public List<Coordinates> getCellLengthCoordinates() {
        return extractCoordinates(cellLengthMode.getShapeList());
    }

    public List<Coordinates> getCropRegionCoordinates() {
        return extractCoordinates(cropMode.getShapeList());
    }

    private static List<Coordinates> extractCoordinates(List<? extends SelectableShape> shapes) {
        final int n = shapes.size();
        final ArrayList<Coordinates> ret = new ArrayList<Coordinates>(n);
        for (SelectableShape s : shapes) {
            ret.add(s.getCoordinates());
        }
        return ret;
    }

    public int getMinimumImageDim() {
        return Math.min(image.getHeight(), image.getWidth());
    }

    public Double[] getCurrentMembraneProfile() {
        final int idx = CellXGuiConfiguration.getInstance().membraneIntensityProfileParameter.getLocation();
        final Object obj = paramTableModel.getValues()[idx];
        if (obj != null && obj instanceof Double[]) {
            return (Double[]) obj;
        }
        return null;
    }

    public Integer getCurrentMembraneLocation() {
        final int idx = CellXGuiConfiguration.getInstance().membraneLocationParameter.getLocation();
        final Object obj = paramTableModel.getValues()[idx];
        if (obj instanceof Integer) {
            return (Integer) obj;
        }
        return null;
    }

    public Integer getCurrentMembraneWidth() {
        final int idx = CellXGuiConfiguration.getInstance().membraneWidthParameter.getLocation();
        final Object obj = paramTableModel.getValues()[idx];
        if (obj instanceof Integer) {
            return (Integer) obj;
        }
        return null;
    }

    public File getCurrentImage() {
        return currentImage;
    }

    public File getFluoTestImage() {
        return fluoTestImage;
    }

    public File getFlatFieldTestImage() {
        return flatFieldTestImage;
    }
}
