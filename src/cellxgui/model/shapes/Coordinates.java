/*
 * MembraneProfileCoordinates.java
 *
 * Copyright (C) 2011 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.model.shapes;

import java.io.Serializable;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class Coordinates implements Serializable{
private static final long serialVersionUID = 1L;
    public final int x1, x2, y1, y2;

    public Coordinates(int x1, int y1, int x2, int y2) {
        this.x1 = x1;
        this.x2 = x2;
        this.y1 = y1;
        this.y2 = y2;
    }

    public double getLength() {
        final double dx = 1 + Math.abs(x1 - x2);
        final double dy = 1 + Math.abs(y1 - y2);
        return Math.sqrt(dx * dx + dy * dy);
    }

    public int getNumberOfPixelsOnConnectingLine() {
        return Math.max(1 + Math.abs(x1 - x2), 1 + Math.abs(y1 - y2 + 1));
    }

    public int getHeight() {
        return Math.abs(y1 - y2);
    }

    public int getWidth() {
        return Math.abs(x1 - x2);
    }

    public int getBoundingBoxHeight() {
        return Math.abs(y1 - y2) + 2;
    }

    public int getBoundinBoxWidth() {
        return Math.abs(x1 - x2) + 2;
    }

    public int getBoundingBoxX() {
        if (x1 < x2) {
            return x1 - 1;
        } else {
            return x2 - 1;
        }
    }

    public int getBoundingBoxY() {
        if (y1 < y2) {
            return y1 - 1;
        } else {
            return y2 - 1;
        }
    }

    public int getLX() {
        if (x1 < x2) {
            return x1;
        } else {
            return x2;
        }
    }

    public int getLY() {
        if (y1 < y2) {
            return y1;
        } else {
            return y2;
        }
    }

    @Override
    public String toString() {
        return String.format("Coordinates( x1=%d y2=%d x2=%d y2=%d pl=%d l=%.1f)", x1, y1, x2, y2, getNumberOfPixelsOnConnectingLine(), getLength());
    }
}
