/*
 * FileSeriesXMLWriter.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.model;

import cellxgui.controller.CellXGuiConfiguration;
import cellxgui.controller.filehandling.CellXFileSet;
import cellxgui.controller.filehandling.CellXFluoFile;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class FileSeriesXMLWriter {

    private boolean wasSuccesful;
    private final File file;
    private List<CellXFileSet> series;

    public FileSeriesXMLWriter(List<CellXFileSet> fileSeries, File f) {
        this.file = f;
        this.series = fileSeries;
    }

    public void writeToFile() {
        wasSuccesful = false;
        try {
            final Document doc = createDocument();
            final Source source = new DOMSource(doc);
            final Result result = new StreamResult(file);
            final Transformer t = TransformerFactory.newInstance().newTransformer();
            t.setOutputProperty(OutputKeys.INDENT, "yes");
            t.setOutputProperty(OutputKeys.STANDALONE, "no");
            t.transform(source, result);
            wasSuccesful = true;
        } catch (TransformerConfigurationException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getMessage());
        } catch (TransformerException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getMessage());
        } catch (ParserConfigurationException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getMessage());
        }
    }

    private Document createDocument() throws ParserConfigurationException {
        final Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();

        final Element root = doc.createElement(CellXGuiConfiguration.CellXFileSeriesXMLRootNodeName);
        final DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd__HH_mm_ss");
        root.setAttribute("timestamp", dateFormat.format(new Date()));
        root.setAttribute("creator", "CellXGui");
        doc.appendChild(root);

        for (CellXFileSet fs : series) {
            buildXmlTreeFor(fs, root, doc);
        }
        return doc;
    }

    public boolean wasSuccessful() {
        return wasSuccesful;
    }

    private void buildXmlTreeFor(CellXFileSet fs, Element root, Document doc) {
        final Element fsNode = doc.createElement(CellXGuiConfiguration.CellXFileSetXMLNodeName);
        root.appendChild(fsNode);
        fsNode.setAttribute(CellXGuiConfiguration.CellXFileSetFrameAttrName, String.valueOf(fs.getFrameIdx()));
        /*    fsNode.setAttribute(
        CellXGuiConfiguration.CellXFileSetPositionAttrName, 
        fs.hasPosition()?String.valueOf(fs.getPosition()):"");
         */
        final Element oofNode = doc.createElement(CellXGuiConfiguration.CellXOofImageXMLNodeName);
        fsNode.appendChild(oofNode);
        oofNode.setTextContent(fs.getOutOfFocusImage().toString());

        for (CellXFluoFile fluoObj : fs.getFluoFiles()) {
            final Element fluoNode = doc.createElement(CellXGuiConfiguration.CellXFluoSetXMLNodeName);
            fsNode.appendChild(fluoNode);
            fluoNode.setAttribute(CellXGuiConfiguration.CellXFluoTypeAttrName, fluoObj.getType());
            final Element fluoImgNode = doc.createElement(CellXGuiConfiguration.CellXFluoImageXMLNodeName);
            fluoNode.appendChild(fluoImgNode);
            fluoImgNode.setTextContent(fluoObj.getFluoImage().toString());
            if (fluoObj.hasFlatFieldImage()) {
                final Element ffNode = doc.createElement(CellXGuiConfiguration.CellXFfImageXMLNodeName);
                ffNode.setTextContent(fluoObj.getFlatFieldImage().toString());
                fluoNode.appendChild(ffNode);
            }
        }
    }
}
