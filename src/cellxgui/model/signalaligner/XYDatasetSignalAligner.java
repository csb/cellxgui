/*
 * XYDatasetSignalAligner.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.model.signalaligner;

import org.jfree.data.DomainOrder;
import org.jfree.data.general.DatasetChangeListener;
import org.jfree.data.general.DatasetGroup;
import org.jfree.data.xy.XYDataset;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class XYDatasetSignalAligner extends MembraneSignalAligner implements XYDataset {

    private DatasetGroup group = new DatasetGroup();

    public XYDatasetSignalAligner(double percentageOfAvgCoverage) {
        super(percentageOfAvgCoverage);
    }

    @Override
    public DomainOrder getDomainOrder() {
        return DomainOrder.ASCENDING;
    }

    @Override
    public int getItemCount(int i) {
        return trimmedSignal.length;
    }

    @Override
    public Number getX(int i, int i1) {
        if (i == 0) {
            return i1;
        }
        return 0;
    }

    @Override
    public double getXValue(int i, int i1) {
        if (i == 0) {
            return i1;
        }
        return 0;
    }

    @Override
    public Number getY(int i, int i1) {
        if (i == 0) {
            return trimmedSignal[i1];
        }
        return 0;
    }

    @Override
    public double getYValue(int i, int i1) {
        if (i == 0) {
            return trimmedSignal[i1];
        }
        return 0;
    }

    @Override
    public int getSeriesCount() {
        return 1;
    }

    @Override
    public Comparable getSeriesKey(int i) {
        return 0;
    }

    @Override
    public int indexOf(Comparable cmprbl) {
        return 0;
    }

    @Override
    public void addChangeListener(DatasetChangeListener dl) {
    }

    @Override
    public void removeChangeListener(DatasetChangeListener dl) {
    }

    @Override
    public DatasetGroup getGroup() {
        return group;
    }

    @Override
    public void setGroup(DatasetGroup dg) {
        this.group = dg;
    }
}
