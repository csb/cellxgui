package cellxgui.model.signalaligner;

import java.util.Arrays;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class SignalAligner {

    protected int[] perPositionSignalCount;
    protected double[] perPositionSignalSum;
    protected double[] currentConsensus;

    public SignalAligner() {
        currentConsensus = new double[0];
        perPositionSignalSum = new double[0];
        perPositionSignalCount = new int[0];
    }

    public void addSignal(Signal s) {
        if (currentConsensus.length == 0) {
            perPositionSignalCount = new int[s.getLength()];
            Arrays.fill(perPositionSignalCount, 1);
            perPositionSignalSum = Arrays.copyOf(s.getNormalizedValues(), s.getLength());
            currentConsensus = Arrays.copyOf(s.getNormalizedValues(), s.getLength());
        } else {
            final int[] pos = findBestAlnPos(s.getNormalizedValues(), currentConsensus);
            resize(pos, s.getLength());
            registerSignal(s.getNormalizedValues(), pos);
            normalize();
        }
    }

    public void reset() {
        currentConsensus = new double[0];
        perPositionSignalSum = new double[0];
        perPositionSignalCount = new int[0];
    }

    private void normalize() {
        double mean = 0.0;
        final int n = currentConsensus.length;
        for (int i = 0; i < n; ++i) {
            currentConsensus[i] = perPositionSignalSum[i] / perPositionSignalCount[i];
            mean += currentConsensus[i];
        }
        mean /= n;
        for (int i = 0; i < n; ++i) {
            currentConsensus[i] -= mean;
        }
    }

    private void registerSignal(double[] s, int[] pos) {
        if (pos[0] == 0) {
            final int offset = pos[1];
            for (int i = 0; i < s.length; ++i) {
                ++perPositionSignalCount[offset + i];
                perPositionSignalSum[offset + i] += s[i];
            }
        } else {
            for (int i = 0; i < s.length; ++i) {
                ++perPositionSignalCount[i];
                perPositionSignalSum[i] += s[i];
            }
        }
    }

    private void resize(int[] pos, int length) {
        if (pos[0] == 0) {
            //check if the signal fits into the current mem
            //otherwise extend right end
            final int n = pos[1] + length;
            if (n > currentConsensus.length) {
                perPositionSignalCount = Arrays.copyOf(perPositionSignalCount, n);
                perPositionSignalSum = Arrays.copyOf(perPositionSignalSum, n);
                currentConsensus = new double[n];
            }
        } else {
            final int offset = pos[0];
            final int n = Math.max(length, offset + currentConsensus.length);

            int tmp1[] = new int[n];
            double tmp2[] = new double[n];
            for (int i = 0; i < perPositionSignalCount.length; ++i) {
                tmp1[offset + i] = perPositionSignalCount[i];
                tmp2[offset + i] = perPositionSignalSum[i];
            }
            perPositionSignalCount = tmp1;
            perPositionSignalSum = tmp2;

            /* for (int i = 0; i < signalAlignmentOffsets.size(); ++i) {
            signalAlignmentOffsets.set(i, signalAlignmentOffsets.get(i) + offset);
            }*/
            currentConsensus = new double[n];
        }
    }

    public double getAverageSignalCoverage() {
        double ret = 0;
        for (int i = 0; i < perPositionSignalCount.length; ++i) {
            ret += perPositionSignalCount[i];
        }
        return ret /= perPositionSignalCount.length;
    }

    public int getMaximumSignalCoverage() {
        int ret = 0;
        for (int i = 0; i < perPositionSignalCount.length; ++i) {
            ret = Math.max(ret, perPositionSignalCount[i]);
        }
        return ret;
    }

    public Double[] getTrimmedSignal(double percentageOfAvgCoverage) {

        final double avg = percentageOfAvgCoverage * getAverageSignalCoverage();
        int start = 0;
        int end = perPositionSignalCount.length - 1;

        for (int i = 0; i < perPositionSignalCount.length; ++i) {
            if (perPositionSignalCount[i] >= avg) {
                start = i;
                break;
            }
        }

        for (int i = perPositionSignalCount.length - 1; i >= 0; --i) {
            if (perPositionSignalCount[i] >= avg) {
                end = i;
                break;
            }
        }
        final Double[] ret = new Double[end - start + 1];
        for (int i = 0; i < ret.length; ++i) {
            ret[i] = currentConsensus[start + i];
        }
        return ret;
    }

    public int getTotalSignalAlignmentLength() {
        return perPositionSignalCount.length;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < currentConsensus.length; ++i) {
            sb.append(String.format("%4d  %6.4f\n", i, currentConsensus[i]));
        }
        return sb.toString();
    }

    public String toOctave() {
        return toOctave(currentConsensus);
    }

    public String toTrimmedOctave() {
        return toOctave(getTrimmedSignal(0.5));
    }

    private static String toOctave(Double[] values) {
        final StringBuilder sb = new StringBuilder();
        sb.append("plot([");
        for (int i = 0; i < values.length; ++i) {
            sb.append(String.format("%6.4f", values[i]));
            if (i < values.length - 1) {
                sb.append(", ");
            }
        }
        sb.append("]);");
        return sb.toString();
    }

    private static String toOctave(double[] values) {
        final StringBuilder sb = new StringBuilder();
        sb.append("plot([");
        for (int i = 0; i < values.length; ++i) {
            sb.append(String.format("%6.4f", values[i]));
            if (i < values.length - 1) {
                sb.append(", ");
            }
        }
        sb.append("]);");
        return sb.toString();
    }

    /**
     * 
     * @param a 
     * @param b 
     * @return 
     */
    public static int[] findBestAlnPos(double[] a, double[] b) {
        final int end = b.length + a.length - 1;
        double bestCor = Double.NEGATIVE_INFINITY;

        int[] bestPos = new int[2];

        int aStart, bEnd, bStart, j, k;


        //System.out.println(String.format("aLength=%d bLength=%d \n", a.length, b.length));

        double d;
        for (int i = 0; i < end; ++i) {
            aStart = Math.max(0, i - b.length + 1);
            bStart = Math.max(0, b.length - i - 1);
            bEnd = bStart + Math.min(a.length - aStart, b.length - bStart);
            //System.out.println(String.format("i=%d   aStart=%d   bStart=%d bEnd=%d  ",i, aStart, bStart, bEnd));
            k = aStart;
            d = 0.0;
            for (j = bStart; j < bEnd; ++j) {
                d += b[j] * a[k++];
            }

            //System.out.println("Correlation value " + d);

            if (d > bestCor) {
                bestCor = d;
                bestPos[0] = aStart;
                bestPos[1] = bStart;
            }

        }
        return bestPos;
    }
}
