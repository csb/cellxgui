package cellxgui.model.signalaligner;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class TestSignal implements Signal {

    final static double[] sig = {-1, -2, -0.8, 0.1, 1.5, 3, 0.4};
    protected double[] values;

    public TestSignal(int maxLen, int minLen) {

        int l = minLen + (int) (Math.random() * (maxLen - minLen));

        int signalPos = (int) (Math.random() * l);


        values = new double[l];
        for (int i = 0; i < l; ++i) {
            values[i] = Math.random() - 0.5;
        }


        for (int i = signalPos; i < Math.min(l, signalPos + 7); ++i) {
            values[i] += sig[i - signalPos];
        }

        double mean = 0.0;
        for (int i = 0; i < l; ++i) {
            mean += values[i];
        }
        mean /= l;

        for (int i = 0; i < l; ++i) {
            values[i] -= mean;
        }

    }

    @Override
    public double[] getNormalizedValues() {
        return values;
    }

    @Override
    public int getLength() {
        return values.length;
    }
}
