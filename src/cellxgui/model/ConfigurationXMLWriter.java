/*
 * ConfigurationXMLWriter.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.model;

import cellxgui.controller.CellXGuiConfiguration;
import cellxgui.controller.CellXParameter;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class ConfigurationXMLWriter {

    private final CellXModel model;
    private boolean wasSuccessful;

    public ConfigurationXMLWriter(CellXModel model) {
        this.model = model;
    }

    public void writeToFile(File f) {
        wasSuccessful = false;
        try {
            final Document doc = createDocument();
            final Source source = new DOMSource(doc);
            final Result result = new StreamResult(f);
            final Transformer t = TransformerFactory.newInstance().newTransformer();
            t.setOutputProperty(OutputKeys.INDENT, "yes");
            t.setOutputProperty(OutputKeys.STANDALONE, "no");
            t.transform(source, result);
            wasSuccessful = true;
        } catch (TransformerConfigurationException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getMessage());
        } catch (TransformerException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getMessage());
        } catch (ParserConfigurationException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getMessage());
        }
    }

    private Document createDocument() throws ParserConfigurationException {
        final Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();

        final Element root = doc.createElement(CellXGuiConfiguration.CellXConfigurationXMLRootNodeTagName);
        final DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd__HH_mm_ss");
        root.setAttribute("timestamp", dateFormat.format(new Date()));
        root.setAttribute("creator", "CellXGui");
        doc.appendChild(root);

        addParams(doc, root, model.getParameterValues(), CellXGuiConfiguration.getParameters());
        addParams(doc, root, model.getAdvancedParameterValues(), CellXGuiConfiguration.getAdvancedParameters());

        return doc;
    }

    public void addParams(Document doc, Element root, Object[] values, List<CellXParameter> params) {
        for (int i = 0; i < values.length; ++i) {
            final CellXParameter p = params.get(i);


            if (values[i] == null && p.has(CellXParameter.PROPERTY_TYPE.OPTIONAL)) {
                continue;
            }

            if (values[i] == null) {
                // System.out.println("Warning: Found null value parameter");
                continue;
            }

            final Element tmp = doc.createElement(CellXGuiConfiguration.CellXConfigurationXMLParamNodeTagName);
            root.appendChild(tmp);


            if (p.equals(CellXGuiConfiguration.getInstance().cropRegionBoundaryParameter)) {
                final Number[] ar = (Number[]) values[i];
                tmp.setAttribute(CellXGuiConfiguration.CellXConfigurationXMLAttrName, p.getName());
                for (int j = 0; j < ar.length; ++j) {
                    final Element arE = doc.createElement(CellXGuiConfiguration.CellXConfigurationXMLArrayElementNodeTagName);
                    tmp.appendChild(arE);
                    arE.setAttribute(CellXGuiConfiguration.CellXConfigurationXMLAttrValue, String.valueOf(ar[j].intValue() + 1));
                }
            } else if (p.isDouble()) {
                final Number n = (Number) values[i];
                tmp.setAttribute(CellXGuiConfiguration.CellXConfigurationXMLAttrName, p.getName());
                tmp.setAttribute(CellXGuiConfiguration.CellXConfigurationXMLAttrValue, String.valueOf(n));
            } else if (p.isInteger()) {
                final Number n = (Number) values[i];
                tmp.setAttribute(CellXGuiConfiguration.CellXConfigurationXMLAttrName, p.getName());
                tmp.setAttribute(CellXGuiConfiguration.CellXConfigurationXMLAttrValue, String.valueOf(n.intValue()));
            } else if (p.isBoolean()) {
                final Boolean b = (Boolean) values[i];
                tmp.setAttribute(CellXGuiConfiguration.CellXConfigurationXMLAttrName, p.getName());
                tmp.setAttribute(CellXGuiConfiguration.CellXConfigurationXMLAttrValue, b ? "1" : "0");
            } else if (p.isIntegerArray()) {
                final Number[] ar = (Number[]) values[i];
                tmp.setAttribute(CellXGuiConfiguration.CellXConfigurationXMLAttrName, p.getName());
                for (int j = 0; j < ar.length; ++j) {
                    final Element arE = doc.createElement(CellXGuiConfiguration.CellXConfigurationXMLArrayElementNodeTagName);
                    tmp.appendChild(arE);
                    arE.setAttribute(CellXGuiConfiguration.CellXConfigurationXMLAttrValue, String.valueOf(ar[j].intValue()));
                }
            } else if (p.isDoubleArray()) {
                final Number[] ar = (Number[]) values[i];
                tmp.setAttribute(CellXGuiConfiguration.CellXConfigurationXMLAttrName, p.getName());
                for (int j = 0; j < ar.length; ++j) {
                    final Element arE = doc.createElement(CellXGuiConfiguration.CellXConfigurationXMLArrayElementNodeTagName);
                    tmp.appendChild(arE);
                    arE.setAttribute(CellXGuiConfiguration.CellXConfigurationXMLAttrValue, String.valueOf(ar[j]));
                }
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Found unexpected parameter type: {0}", p);
            }

        }
    }

    public boolean wasSuccessful() {
        return wasSuccessful;
    }
}
