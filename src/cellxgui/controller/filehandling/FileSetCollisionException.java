/*
 * FileSetCollisionException.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.controller.filehandling;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class FileSetCollisionException extends Exception {

    private final CellXFileSet existing;
    private final CellXFileSet other;

    FileSetCollisionException(CellXFileSet existing, CellXFileSet other) {
        this.existing = existing;
        this.other = other;
    }

    public CellXFileSet getExisting() {
        return existing;
    }

    public CellXFileSet getOther() {
        return other;
    }
}
