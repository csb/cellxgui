/*
 * NoOutOfFocusImagesFoundException.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 */
package cellxgui.controller.filehandling;

import java.util.regex.Pattern;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class NoOutOfFocusImagesFoundException extends Exception {

    private final Pattern oof, frame;

    public NoOutOfFocusImagesFoundException(Pattern oof, Pattern frame) {
        this.oof = oof;
        this.frame = frame;
    }

    public Pattern getOofPattern() {
        return oof;
    }

    public Pattern getFramePattern() {
        return frame;
    }
}
