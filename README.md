Info
=====
CellX was developed at the CSB group [[1]], and consists of the CellX executable as well as this GUI.
Details on the usage of CellX for segmentation are provided in [[2]], more information on the method can be found in [[3]].

License
-------
CellX is provided under the [simplified BSD license](LICENSE.txt).

Authors
-------
    Christian Mayer <chrixian.mayer@gmail.com>
    Sotiris Dimopoulos <sotiris.dimopoulos@gmail.com>

Current Maintainer:
    
    Lukas Widmer <lukas.widmer@bsse.ethz.ch>
    Andreas Cuny <andreas.cuny@bsse.ethz.ch>

References
----------

[1]: http://www.csb.ethz.ch/tools/software/cellx.html
1. [CellX documentation on the CSB group website][1]
[2]: http://doi.org/10.1002/0471142727.mb1422s101
2. Mayer C, Dimopoulos S, Rudolf F, Stelling J (2013)  
Using CellX to quantify intracellular events  
[Curr Protoc Mol Biol Chapter 14: Unit 14 22.][2]
[3]: http://doi.org/10.1093/bioinformatics/btu302
3. Dimopoulos S, Mayer CE, Rudolf F, Stelling J (2014)  
Accurate cell segmentation in microscopy images using membrane patterns.  
[Bioinformatics 30: 2644-2651.][3]