/*
 * CellXTimeSeriesGeneratorTest.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXGui
 *
 * CellXGuiis free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * CellXGui is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package cellxgui.controller.filehandling;

import cellxgui.model.TimeSeriesXMLWriter;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class CellXTimeSeriesGeneratorTest {

    public CellXTimeSeriesGeneratorTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Test
    public void testSomeMethod() {


        final File dir = new File("/links/groups/stelling/wetlab/Microscopy/RawImageData/ph_fluorophores/yfpglucosepulse");
        final Pattern outFoFocusPattern = Pattern.compile(".*?BFout_.*?\\.tif$");

        final List<Pattern> fluoPatterns = new ArrayList<Pattern>();
        fluoPatterns.add(Pattern.compile(".*?(RFP)_.*?\\.tif$"));
        fluoPatterns.add(Pattern.compile(".*?(GFP)_.*?\\.tif$"));
        fluoPatterns.add(Pattern.compile(".*?(YFP)_.*?\\.tif$"));
        final Pattern frameIdxPattern = Pattern.compile(".*?_time(\\d+).+");
        final Pattern positionPattern = Pattern.compile(".*?_position(\\d+).+");

        final Pattern ffPattern = Pattern.compile(".*?FF(GFP|RFP).*");

        final File resultDir = new File("/results");

        final CellXTimeSeriesGenerator tgen = new CellXTimeSeriesGenerator(
                dir,
                resultDir,
                outFoFocusPattern,
                fluoPatterns,
                frameIdxPattern,
                ffPattern, true);
        try {
            List<CellXTimeSeries> l = tgen.getTimeSeries();
            TimeSeriesXMLWriter w = new TimeSeriesXMLWriter();
            w.writeAllSeriesToFile(new File("/home/cmayer/timeseries.xml"), l);
            for (CellXTimeSeries ts : l) {
                System.out.println(ts);
            }
        } catch (FileSetCollisionException e) {

            System.out.println(e.getExisting());
            System.out.println(e.getOther());
            e.printStackTrace();
        } catch (NoOutOfFocusImagesFoundException e) {

            System.out.println(e);
            e.printStackTrace();
        }
    }

}
